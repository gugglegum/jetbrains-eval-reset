<?php

declare(strict_types=1);

namespace App;

class ConfigDir
{
    /**
     * @param string $configDir
     * @return bool
     */
    public static function isValidConfigDir(string $configDir): bool
    {
        return is_dir($configDir) && file_exists($configDir . '/options/other.xml');
    }

    public static function getBackupDir(string $configDir): string
    {
        return $configDir . '-backup';
    }
}