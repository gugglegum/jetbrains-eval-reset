<?php

declare(strict_types=1);

namespace App\Utils;

final class Console
{
    private static \PHP_Parallel_Lint\PhpConsoleColor\ConsoleColor $consoleColor;

    /**
     * @return \PHP_Parallel_Lint\PhpConsoleColor\ConsoleColor
     */
    protected static function consoleColor(): \PHP_Parallel_Lint\PhpConsoleColor\ConsoleColor
    {
        if (!isset(self::$consoleColor)) {
            self::$consoleColor = new \PHP_Parallel_Lint\PhpConsoleColor\ConsoleColor();
        }
        return self::$consoleColor;
    }

    public static function debug(string $text)
    {
        self::styledOutput('dark_gray', $text);
    }
    public static function comment(string $text)
    {
        self::styledOutput('dark_gray', $text);
    }
    public static function info(string $text)
    {
        self::styledOutput('green', $text);
    }
    public static function warning(string $text)
    {
        self::styledOutput('yellow', $text);
    }
    public static function error(string $text)
    {
        self::styledOutput('light_red', $text);
    }

    public static function title(string $text)
    {
        self::styledOutput(['bold', 'white'], $text);
    }

    public static function subtitle(string $text)
    {
        self::styledOutput('light_yellow', $text);
    }

    public static function description(string $text)
    {
        self::styledOutput('cyan', $text);
    }

    public static function text(string $text)
    {
        echo $text, "\n";
    }

    protected static function styledOutput($style, string $text)
    {
        try {
            echo self::consoleColor()->apply($style, $text) . "\n";
        } catch (\Exception $e) {
            throw new \RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }

    /**
     * Confirmation dialog with question where user must enter Yes or No. Has default option which is used on empty
     * user input.
     *
     * @param string $question
     * @param bool|null $default
     * @return bool
     */
    public static function confirm(string $question, ?bool $default = null): bool
    {
        do {
            $input = self::ask($question,
                $default !== null ? ($default ? 'yes' : 'no') : null,
                null,
                'y/n',
            );
            switch (strtolower($input)) {
                case '':
                    if ($default !== null) {
                        return $default;
                    }
                    break;
                case 'y':
                case 'yes':
                    return true;
                case 'n':
                case 'no':
                    return false;
                default:
                    self::error("Unexpected input \"{$input}\"");
            }
        } while(true);
    }

    /**
     * Ask question from user. User can enter any input. Has default options which is used on empty user input.
     *
     * @param string $question
     * @param string|null $default
     * @param callable|null $validate
     * @param string|null $hint
     * @return string
     */
    public static function ask(string $question, ?string $default = null, ?callable $validate = null, string $hint = null): string
    {
        self::info($question);
        do {
            $input = readline(($hint != '' ? " [{$hint}]" : '') . ($default != '' ? " ($default)" : '') . ' > ');
            if (is_callable($validate)) {
                if (!$validate($input)) {
                    continue;
                }
            }
            break;
        } while (true);
        return $input;
    }

    /**
     * The same as ask() but with storing entered input in history so user can reuse previous input by pressing
     * arrow up key.
     *
     * @param string $question
     * @param string|null $default
     * @param callable|null $validate
     * @param string|null $hint
     * @return string
     */
    public static function askWithHistory(string $question, ?string $default = null, ?callable $validate = null, string $hint = null): string
    {
        $input = self::ask($question, $default, $validate, $hint);
        readline_add_history($input);
        return $input;
    }
}
