<?php

declare(strict_types=1);

namespace App\Utils;

final class Exec
{
    /**
     * @param string $command
     * @throws \Exception
     */
    public static function command(string $command)
    {
        $retCode = self::query($command);
        if ($retCode !== 0) {
            throw new \Exception("External command ({$command}) returned invalid result code ({$retCode})");
        }
    }

    /**
     * @param string $command
     * @return int
     * @throws \Exception
     */
    public static function query(string $command): int
    {
//        Console::debug("> {$command}\n");
        $process = proc_open($command, [
            1 => ["pipe", "w"],
            2 => ["pipe", "w"],
        ], $pipes);

        if (is_resource($process)) {
            stream_get_contents($pipes[1]);
            fclose($pipes[1]);
            stream_get_contents($pipes[2]);
            fclose($pipes[2]);

            return proc_close($process);
        } else {
            throw new \Exception("Failed to execute command: {$command}");
        }
    }

}
