<?php

declare(strict_types=1);

namespace App\Steps;

interface StepInterface
{
    public function getDescription(): string;

    /**
     * Performs actions of the step
     */
    public function execute();

    /**
     * Reverts actions of the step previously performed by execute() method
     */
    public function revert();

    /**
     * Checks whether revert needed
     *
     * @return bool
     */
    public function isNeedRevert(): bool;
}
