<?php

declare(strict_types=1);

namespace App\Steps;

use App\ConfigDir;
use App\Exceptions\UserAbortException;
use App\Utils\Console;
use App\Utils\File;

class Step1 extends StepAbstract
{
    /**
     * @var bool
     */
    private bool $isConfigMoved = false;

    public function getDescription(): string
    {
        return "Make backup";
    }

    /**
     * Performs actions of the step
     *
     * @throws UserAbortException
     * @throws \Exception
     */
    public function execute()
    {
        if (is_dir($this->config->getConfigDir())) {
            if (is_dir($this->config->getBackupDir())) {
                if (ConfigDir::isValidConfigDir($this->config->getBackupDir()) && File::isEmptyDir($this->config->getConfigDir())) {
                    if (!Console::confirm("Looks like previous execution was aborted - config already moved to backup directory. Continue?")) {
                        throw new UserAbortException();
                    }
                    $this->isConfigMoved = true;
                } elseif (ConfigDir::isValidConfigDir($this->config->getBackupDir()) && ConfigDir::isValidConfigDir($this->config->getConfigDir())) {
                    $input = Console::ask("Looks like previous execution was aborted - both config and backup directories are not empty. Which version you want to use: backup or current?",
                        null,
                        function(string $value) { return in_array(strtolower($value), ['backup', 'current', 'abort']); },
                        'backup/current/abort');
                    switch ($input) {
                        case 'backup':
                            Console::debug("Cleaning config directory");
                            File::deleteDir($this->config->getConfigDir(), true);
                            $this->isConfigMoved = true;
                            break;
                        case 'current':
                            Console::debug("Cleaning backup directory");
                            File::deleteDir($this->config->getBackupDir(), true);
                            break;
                        default:
                            throw new UserAbortException();
                    }
                }

            } else {
                Console::debug('Creating backup directory');
                File::createDir($this->config->getBackupDir(), 0700);
            }

            if (!$this->isConfigMoved) {
                Console::info("We are about to move the config folder to the backup. {$this->config->getProductName()} must be closed.\n");
                if (!Console::confirm('Are you ready?', true)) {
                    throw new UserAbortException();
                }

                Console::debug('Moving the config folder to backup');
                try {
                    File::moveDir($this->config->getConfigDir(), $this->config->getBackupDir(), true);
                    $this->isConfigMoved = true;
                } catch (\RuntimeException $e) {
                    throw $e;
                }
            }
        } else {
            Console::error("No config directory exists");
            if (ConfigDir::isValidConfigDir($this->config->getBackupDir())) {
                Console::info("But we have some config in backup directory.\nLooks like you've already backed up config and just aborted.");
                if (!Console::confirm('Would you like to continue?')) {
                    throw new UserAbortException();
                }
                Console::info("Creating empty config directory");
                File::createDir($this->config->getConfigDir());
                $this->isConfigMoved = true;
            } else {
                throw new \Exception("No config directory found");
            }
        }
    }

    /**
     * Reverts actions of the step previously performed by execute() method
     */
    public function revert()
    {
        if ($this->isConfigMoved) {
            Console::debug("\tMoving config folder back from backup");
            try {
                File::moveDir($this->config->getBackupDir(), $this->config->getConfigDir(), true);
                $this->isConfigMoved = false;
                File::rmdir($this->config->getBackupDir());
            } catch (\RuntimeException $e) {
                throw $e;
            }
        }
    }

    public function isNeedRevert(): bool
    {
        return $this->isConfigMoved;
    }
}
