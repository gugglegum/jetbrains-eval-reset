<?php

declare(strict_types=1);

namespace App\Steps;

use App\Config;

abstract class StepAbstract implements StepInterface
{
    /**
     * @var Config
     */
    protected Config $config;

    public function __construct(Config $stepConfig)
    {
        $this->config = $stepConfig;
    }

    abstract public function getDescription(): string;
}
