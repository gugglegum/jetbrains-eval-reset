<?php

declare(strict_types=1);

namespace App\Steps;

use App\Exceptions\UserAbortException;
use JetBrains\PhpStorm\Pure;
use App\Utils\Console;
use App\Utils\File;

class Step3 extends StepAbstract
{
    public function getDescription(): string
    {
        return "New evaluate period and merge with old config";
    }

    /**
     * Performs actions of the step
     *
     * @throws UserAbortException
     */
    public function execute()
    {
        Console::info("Now start {$this->config->getProductName()} and do the following things:\n"
            . " - Select (*) Evaluate for free\n"
            . " - Press [Evaluate]\n"
            . " - Press [Exit]\n");
        do {
            if (Console::confirm('Did it?', true)) {
                if (!File::isEmptyDir($this->config->getConfigDir() . DIRECTORY_SEPARATOR . 'eval')) {
                    break;
                } else {
                    Console::warning("No, you didn't.");
                }
            } else {
                if (Console::confirm("Do it, man. Or do you want to exit?")) {
                    throw new UserAbortException();
                }
            }
        } while (true);

        $optionsFile = 'options/other.xml';

        //
        // Copying all other config files
        //

        Console::debug('Copying old config files from backup');
        File::copyDir($this->config->getBackupDir(), $this->config->getConfigDir(), true,
            function(\SplFileInfo $entry) use ($optionsFile) {
                return $entry->getRealPath() !== realpath($this->config->getBackupDir() . DIRECTORY_SEPARATOR . $optionsFile)
                    && realpath($entry->getPath()) !== realpath($this->config->getBackupDir() . DIRECTORY_SEPARATOR . 'eval');

            });

        //
        // Merging old options/other.xml with new one
        //

        Console::debug("Copying and cleaning {$optionsFile} file");
        self::copyAndCleanOptionsFile(
            $this->config->getBackupDir() . DIRECTORY_SEPARATOR . $optionsFile,
            $this->config->getConfigDir() . DIRECTORY_SEPARATOR . $optionsFile
        );

        Console::debug("Removing backup dir");
        File::deleteDir($this->config->getBackupDir());
    }

    /**
     * Reverts actions of the step previously performed by execute() method
     *
     * @throws UserAbortException
     */
    public function revert()
    {
        if (!File::isEmptyDir($this->config->getConfigDir())) {
            Console::debug("\nRemoving newly created config directory");
            File::deleteDir($this->config->getConfigDir(), true);
        }
    }

    /**
     * Copy and clean options/other.xml file
     *
     * @param string $srcOptionsFile
     * @param string $dstOptionsFile
     */
    private static function copyAndCleanOptionsFile(string $srcOptionsFile, string $dstOptionsFile)
    {
        $fSrc = fopen($srcOptionsFile, 'r');
        $fDst = fopen($dstOptionsFile, 'w');

        while (!feof($fSrc)) {

            if (($str = fgets($fSrc)) === false) {
                throw new \RuntimeException("Failed to read from file \"{$srcOptionsFile}\"");
            }
            if (preg_match('/^\s*<property\s+name=\"([^\"]+)\"/', $str, $m)) {
                if (preg_match('/^evlsprt/', $m[1])) {
                    continue;
                }
            }
            if (fputs($fDst, $str) === false) {
                throw new \RuntimeException("Failed to write to file \"{$dstOptionsFile}\"");
            }
        }

        fclose($fSrc);
        fclose($fDst);
    }

    #[Pure] public function isNeedRevert(): bool
    {
        return is_dir($this->config->getConfigDir());
    }
}
