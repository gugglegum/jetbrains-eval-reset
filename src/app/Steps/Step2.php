<?php

declare(strict_types=1);

namespace App\Steps;

use App\Exceptions\UserAbortException;
use JetBrains\PhpStorm\Pure;
use App\Utils\Console;
use App\Utils\Exec;
use App\Utils\File;

class Step2 extends StepAbstract
{
    /**
     * Whether "reg export" command to backup currently existing registry key was successful?
     * @var bool
     */
    private bool $isRegistryKeyExported = false;
    private bool $isRegistryKeyDeleted = false;

    private bool $isJavaUserPrefsDeleted = false;

    public function getDescription(): string
    {
        return "Clean Java user preferences";
    }

    /**
     * Performs actions of the step
     *
     * @throws UserAbortException
     * @throws \Exception
     */
    public function execute()
    {
        if (self::isWindowsOS()) {
            $javaUserPrefsRegistryKey = $this->getJavaUserPrefsRegistryKey();
            if (!Console::confirm("Remove Registry key \"{$javaUserPrefsRegistryKey}\" containing info about previous evaluation?", true)) {
                throw new UserAbortException();
            }
            if (Exec::query('reg query ' . escapeshellarg($javaUserPrefsRegistryKey)) == 0) {
                Console::debug("Making backup of registry");
                try {
                    Exec::command('reg export ' . escapeshellarg($javaUserPrefsRegistryKey) . ' ' . escapeshellarg($this->getJavaUserPrefsRegistryBackupFile()) . ' /y');
                    $this->isRegistryKeyExported = true;
                } catch (\Exception $e) {
                    Console::error("Failed to backup registry key - {$e->getMessage()}");
                }
                Console::debug("Deleting Registry key");
                try {
                    Exec::command('reg delete ' . escapeshellarg($javaUserPrefsRegistryKey) . ' /f');
                    $this->isRegistryKeyDeleted = true;
                } catch (\Exception $e) {
                    Console::error("Failed to delete registry key - {$e->getMessage()} (probably already deleted)");
                }
            } else {
                Console::debug("Registry key doesn't exists, skipping this step");
            }
        } else {
            $javaUserPrefsPath = $this->getJavaUserPrefsPath();
            if (is_dir(self::expandTildeToHomeDir($javaUserPrefsPath))) {
                if (!Console::confirm('Remove folder ' . $javaUserPrefsPath . 'containing info about previous evaluation?', true)) {
                    throw new UserAbortException();
                }

                Console::debug("Making backup");
                File::copyDir(self::expandTildeToHomeDir($javaUserPrefsPath), $this->getJavaUserPrefsBackupPath(), true);

                Console::debug("Removing");
                File::deleteDir(self::expandTildeToHomeDir($javaUserPrefsPath));
                $this->isJavaUserPrefsDeleted = true;
            } else {
                Console::comment("Java user preferences directory {$javaUserPrefsPath} doesn't exists");
            }
        }
    }

    /**
     * Reverts actions of the step previously performed by execute() method
     *
     * @throws \Exception
     */
    public function revert()
    {
        if (self::isWindowsOS()) {
            if ($this->isRegistryKeyDeleted && $this->isRegistryKeyExported) {
                Console::debug("\tRestoring Registry key");
                Exec::command('reg import ' . escapeshellarg($this->getJavaUserPrefsRegistryBackupFile()));
                $this->isRegistryKeyDeleted = false;
            }
        } else {
            if ($this->isJavaUserPrefsDeleted) {
                Console::debug("Restoring from backup");
                File::copyDir($this->getJavaUserPrefsBackupPath(), self::expandTildeToHomeDir($this->getJavaUserPrefsPath()), true);

                Console::debug("Cleaning backup");
                File::deleteDir($this->getJavaUserPrefsBackupPath());
                $this->isJavaUserPrefsDeleted = false;
            }
        }
    }

    #[Pure] private function getJavaUserPrefsRegistryKey(): string
    {
        return 'HKEY_CURRENT_USER\\SOFTWARE\\JavaSoft\\Prefs\\jetbrains\\' . strtolower($this->config->getProductName());
    }

    #[Pure] private function getJavaUserPrefsPath(): string
    {
        return '~/.java/.userPrefs/jetbrains/' . strtolower($this->config->getProductName());
    }

    #[Pure] private function getJavaUserPrefsRegistryBackupFile(): string
    {
        return $this->config->getBackupDir() . '/' . strtolower($this->config->getProductName()) . '.reg';
    }

    #[Pure] private function getJavaUserPrefsBackupPath(): string
    {
        return $this->config->getBackupDir() . '/' . strtolower($this->config->getProductName());
    }

    public function isNeedRevert(): bool
    {
        if (self::isWindowsOS()) {
            return $this->isRegistryKeyDeleted && $this->isRegistryKeyExported;
        } else {
            return $this->isJavaUserPrefsDeleted;
        }
    }

    /**
     * [UNIX only] Expands dir like ~/.java to /home/username/.java
     *
     * @param string $homeRelativePath
     * @return string
     */
    private static function expandTildeToHomeDir(string $homeRelativePath): string
    {
        return preg_replace('/^~\//', getenv('HOME') . '/', $homeRelativePath);
    }

    private static function isWindowsOS(): bool
    {
        return strtoupper(substr(PHP_OS, 0, 3)) === 'WIN';
    }

}
