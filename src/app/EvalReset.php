<?php

namespace App;

use App\Exceptions\Exception;
use App\Exceptions\NormalExitException;
use App\Exceptions\UserAbortException;
use App\Steps\StepInterface;
use App\Utils\Console;

class EvalReset
{
    private Config $config;

    /** @var StepInterface[] */
    private array $steps;

    private int $currentStep = -1;

    private array $products = [
        'AppCode',
        'CLion',
        'DataGrip',
        'GoLand',
        'PhpStorm',
        'Rider',
        'RubyMine',
        'WebStorm',
    ];

    public static function main()
    {
        $app = new self();
        $app->run();
    }

    public function __construct()
    {
        $this->config = new Config();
        $this->steps = [
            new \App\Steps\Step1($this->config),
            new \App\Steps\Step2($this->config),
            new \App\Steps\Step3($this->config),
        ];
    }

    public function run()
    {
        Console::title("JetBrains Evaluation Reset\n==========================\n");
        Console::description("This utility will reset an evaluation period of your JetBrains product installation without losing your settings.\n");

        try {
            $this->initConfig();

            Console::text("Config dir in \"{$this->config->getConfigDir()}\"");
            $this->config->setBackupDir(ConfigDir::getBackupDir($this->config->getConfigDir()));
            Console::text("Backup dir in \"{$this->config->getBackupDir()}\"\n");

            if (!Console::confirm("Ready to start?", true)) {
                throw new UserAbortException();
            }

            while ($this->currentStep < count($this->steps) - 1) {
                $this->currentStep++;
                Console::subtitle("\nStep " . ($this->currentStep + 1) . ": {$this->steps[$this->currentStep]->getDescription()}\n");
                $this->steps[$this->currentStep]->execute();
            }

            Console::warning("\nAll is done. Now you can start your JetBrains product and continue to use it yet another 30 days! :)");
        } catch (\Throwable $e) {
            self::printException($e);
            if (self::isNeedStepsRevert()) {
                Console::debug("...Reverting changes");
                try {
                    while ($this->currentStep >= 0) {
                        if ($this->steps[$this->currentStep]->isNeedRevert()) {
                            $this->steps[$this->currentStep]->revert();
                        }
                        $this->currentStep--;
                    }
                } catch (\Throwable $e) {
                    self::printException($e);
                }
            }
            $exitCode = $e instanceof NormalExitException ? 0 : -1;
            exit($exitCode);
        }
    }

    /**
     * @throws Exception
     */
    private function initConfig()
    {
        $this->parseCommandLineArguments();
        if (!isset($this->configDir)) {
            $this->selectConfigDir();
        }
    }

    /**
     * @throws Exception|NormalExitException
     */
    private function parseCommandLineArguments()
    {
        if ($_SERVER['argc'] > 1) {
            if (in_array($_SERVER['argv'][1], ['help', '/help', '--help', '/?', '-?', '/h', '-h']) || $_SERVER['argc'] > 3) {
                $this->help();
            } else {
                $this->config->setConfigDir(rtrim($_SERVER['argv'][1], '/\\'));
                $this->checkIsValidConfigDir();
                $this->config->setProductName($_SERVER['argv'][2]);
            }
        }
    }

    /**
     * @throws NormalExitException
     */
    private function help()
    {
        Console::info("\nUsage:\n\tjetbrains-evaluation-reset [<Config-Dir> <Product-Name>]\n");
        throw new NormalExitException();
    }

    /**
     * @throws Exception
     */
    private function selectConfigDir()
    {
        if (getenv('APPDATA') != '') {
            $jetbrainsDir = getenv('APPDATA') . DIRECTORY_SEPARATOR . 'JetBrains';

            Console::comment("Scanning your {$jetbrainsDir}");

            $productVersions = $this->getInstalledProductsWithLatestVersions($jetbrainsDir);
            $this->printProductSelectMenu($productVersions);

            Console::ask(
                "\nEvaluation period of which product you would like to reset:",
                null,
                // Validate callback function
                function(string $input) use ($productVersions, $jetbrainsDir): bool {
                    if (preg_match('/^\d+$/', $input)) { // if input is a number
                        $variant = (int) $input - 1;
                        if (array_key_exists($variant, $productVersions)) {
                            Console::info("\nYou have selected the product \"{$productVersions[$variant]['product']}\", version \"{$productVersions[$variant]['version']}\"\n");
                            $this->config->setConfigDir($jetbrainsDir . '/' . $productVersions[$variant]['product'].$productVersions[$variant]['version']);
                            $this->config->setProductName($productVersions[$variant]['product']);
                            return true;
                        } elseif ($variant == -1) {
                            throw new UserAbortException();
                        } else {
                            Console::error("Entered non-existing menu item");
                        }
                    } else {
                        Console::error("Invalid input \"{$input}\" - enter a number of the product");
                    }
                    return false;
                },
                'Menu item'
            );
        } else {
            throw new Exception("No %APPDATA% environment variable defined - can't find JetBrains configs directory");
        }
    }

    /**
     * Returns array with latest versions of products from existing config folders in following format:
     * [
     *   ['product' => 'CLion', 'version' => '2021.1'],
     *   ['product' => 'PhpStorm', 'version' => '2021.1'],
     *   ...
     * ]
     *
     * @param string $jetbrainsDir
     * @return array
     * @throws Exception
     */
    private function getInstalledProductsWithLatestVersions(string $jetbrainsDir): array
    {
        if (($dh = opendir($jetbrainsDir)) !== false) {
            $productVersions = [];
            while (($entry = readdir($dh)) !== false) {
                if ($entry == '.' || $entry == '..') {
                    continue;
                }
                $productDir = $jetbrainsDir . DIRECTORY_SEPARATOR . $entry;
                if (is_dir($productDir) && ConfigDir::isValidConfigDir($productDir)) {
                    foreach ($this->products as $product) {
                        if (preg_match('/^(' . preg_quote($product) . ')(20\d\d\.\d)(?:-backup)?$/', $entry, $m)) {
                            if (!array_key_exists($product, $productVersions) || $productVersions[$product]['version'] < $m[2]) {
                                $productVersions[$product] = [
                                    'product' => $m[1],
                                    'version' => $m[2],
                                ];
                            }
                        }
                    }
                }
            }
            closedir($dh);
            return array_values($productVersions);
        } else {
            throw new Exception("Looks like you don't have any JetBrains product installed (failed to read contents of dir {$jetbrainsDir})");
        }
    }

    private function printProductSelectMenu(array $productVersions)
    {
        Console::info("\nFound " . count($productVersions) . " Jetbrains products:\n");
        foreach ($productVersions as $index => $productAndVersion) {
            Console::text(($index + 1) . ") {$productAndVersion['product']} - {$productAndVersion['version']}");
        }
        Console::text("0) Exit...");
    }

    /**
     * @throws Exception
     */
    private function checkIsValidConfigDir()
    {
        if (!ConfigDir::isValidConfigDir($this->config->getConfigDir())) {
            throw new Exception("Directory {$this->config->getConfigDir()} is not looks like valid config directory\n");
        }
    }

    public static function printException(\Throwable $e)
    {
        if ($e instanceof UserAbortException || $e instanceof NormalExitException) {
            Console::debug($e->getMessage());
        } else {
            Console::error("Error: {$e->getMessage()}\n");
            Console::debug("File: {$e->getFile()} (Line {$e->getLine()})\n");
            Console::debug("Backtrace:\n" . $e->getTraceAsString() . "\n");
        }
    }

    /**
     * Checks whether we need to restore changes we have made
     *
     * @return bool
     */
    private function isNeedStepsRevert(): bool
    {
        while ($this->currentStep >= 0) {
            if ($this->steps[$this->currentStep]->isNeedRevert()) {
                return true;
            }
            $this->currentStep--;
        }
        return false;
    }
}
