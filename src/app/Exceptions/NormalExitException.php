<?php

declare(strict_types=1);

namespace App\Exceptions;

use JetBrains\PhpStorm\Pure;

class NormalExitException extends \RuntimeException
{
    #[Pure] public function __construct()
    {
        parent::__construct('Normal exit');
    }
}