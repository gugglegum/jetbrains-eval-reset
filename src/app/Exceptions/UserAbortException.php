<?php

declare(strict_types=1);

namespace App\Exceptions;

use JetBrains\PhpStorm\Pure;

class UserAbortException extends \RuntimeException
{
    #[Pure] public function __construct()
    {
        parent::__construct('Abort by user');
    }
}
