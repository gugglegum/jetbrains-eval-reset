<?php

declare(strict_types=1);

namespace App;

class Config
{
    private string $productName;

    private string $configDir;

    private string $backupDir;

    /**
     * StepsConfig constructor
     *
     * @param array $data
     */
    public function __construct(array $data = [])
    {
        if ($unexpectedKeys = array_diff(array_keys($data), array_keys(get_class_vars(self::class)))) {
            throw new \LogicException('Unexpected following key(s) in data array passed to the constructor of '
                . __CLASS__ . ': ' . implode(',', $unexpectedKeys));
        }

        foreach (get_class_vars(self::class) as $key => $value) {
            if (array_key_exists($key, $data)) {
                $this->{$key} = $data[$key];
            }
        }
    }

    public function getProductName(): string
    {
        return $this->productName;
    }
    public function setProductName(string $productName): void
    {
        $this->productName = $productName;
    }

    public function getConfigDir(): string
    {
        return $this->configDir;
    }
    public function setConfigDir(string $configDir)
    {
        $this->configDir = $configDir;
    }

    public function getBackupDir(): string
    {
        return $this->backupDir;
    }
    public function setBackupDir(string $backupDir): void
    {
        $this->backupDir = $backupDir;
    }
}
