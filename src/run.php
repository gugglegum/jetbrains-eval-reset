#!/usr/bin/env php
<?php

/*
 * PhpStorm Reset Trial utility
 *
 * Just run this script as follow (correct path to PhpStorm installation dir if needed):
 *
 *      src bin/phpstorm_reset_trial "C:\Program Files\JetBrains\PhpStorm 2018"
 *
 */

if (PHP_SAPI !== 'cli') {
    echo 'Warning: this command must be invoked via the CLI version of PHP, not the ' . PHP_SAPI . ' SAPI' . "\n";
}

require_once __DIR__ . '/vendor/autoload.php';

\App\EvalReset::main();
